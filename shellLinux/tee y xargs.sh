#!/bin/bash
echo Texto de ejemplo para guardar en 2 archivos al mismo tiempo. | tee ficherodeejemplo1.txt ficherodeejemplo2.txt
echo Escritos "ficherodeejemplo1.txt" y "ficherodeejemplo2.txt".
echo Contenido de "ficherodeejemplo1.txt":
cat ficherodeejemplo1.txt
echo Contenido de "ficherodeejemplo2.txt":
cat ficherodeejemplo2.txt
echo Contenido de la carpeta antes de eliminar:
ls
ls fichero*.txt | xargs rm
echo Eliminados "ficherodeejemplo1.txt" y "ficherodeejemplo2.txt".
echo Contenido de la carpeta después de eliminar:
ls
