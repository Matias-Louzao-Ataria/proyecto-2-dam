#!/bin/bash
echo Se buscan los arhivos modificados hace exactamente 1 minuto.;
find -type f -cmin 1;
echo Se buscan los arhivos modificados hace más de 1 minuto.;
find -type f -cmin +1;
echo Se buscan los archivos con un tamaño menor a 4 megabytes.;
find -type f -size -4M;
echo se buscan los directorios que hayan sido modificados hace menos de 5 minutos.
find -type d -cmin -5;
