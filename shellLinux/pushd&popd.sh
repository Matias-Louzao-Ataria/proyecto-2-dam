#!/bin/bash
cd ~;
echo Se añade el directorio de descargas a la pila de directorios, sin cambiar de directorio.;
pushd -n ~/Descargas;
echo Se añade documentos a la pila de directorios y se cambia de directorio.;
pushd ~/Documentos;
echo Se entra al directorio descargas, referenciandolo a través de la pila.;
pushd +1;
echo Se añade el directorio inventado a la pila de directorios, sin cambiar de directorio, ya que puede ser que no exista.;
pushd -n ~/Inventado;
echo Se elimina el directorio inventado, referenciandolo a través de la pila.;
popd +1;
echo Se elimina de la pila el primer directorio de la misma, en este caso documentos.;
popd;
echo Se borran el resto de directorios de la pila.;
popd;
echo Se muestra la pila.;
dirs -v;
