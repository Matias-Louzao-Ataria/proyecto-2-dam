# Proyecto 2°DAM
- [x] [Curso de shell en Linux](#shell-en-linux)
- [x] [Criptografía Simétrica y Asimétrica en la practica](#criptografía-simétrica-y-asimétrica-en-la-practica)
- [x] [Librerías para Web Scraping](#librerías-para-web-scraping)
- [x] [Curso de Web scraping: Extracción de datos en la Web](#curso-de-web-scraping-extracción-de-datos-en-la-web)
- [x] [Curso de introducción al testing](#curso-de-introducción-al-testing)
- [x] [Herramientas de testing para un desarrollador Java](#herramientas-de-testing-para-un-desarrollador-java)
- [x] [Curso de TDD: Test-driven development](#curso-de-tdd-test-driven-development)
- [x] [Experto en bases de datos](#Experto-en-bases-de-datos)
    - [x] [Curso de SQL desde Cero](#curso-sql-desde-cero)
    - [x] [Curso de creación y administración de Bases de Datos SQL](#creación-y-administración-de-bases-de-datos)
    - [x] [Curso de PostgreSQL: Instalación, configuración y optimización](#curso-de-postgresql-instalación-configuración-y-optimización)
    - [x] [Curso de Cassandra](#curso-de-cassandra)
    - [x] [Curso de MongoDB: Creación y gestión de bases de datos NoSQL](#curso-de-mongodb-creación-y-gestión-de-bases-de-datos-nosql)

## Shell en Linux
__Curso que explica conceptos básicos y esenciales a la hora de utilizar la shell de linux__, informa de la existencia de comandos úiles como [`man`](https://wiki.archlinux.org/title/Man_page) y ilustra ulizando ejemplos practicos comando como `rm` o `ls`.
En el curso se explican desde comando básicos como `touch` o `less` hasta comandos más complicados y versátiles como [`tee`](https://wiki.archlinux.org/title/Tee) o `xargs`, además se explican conceptos útiles como:
* Tuberias
* Salida estandar
* Salida de error estandar
* Entrada estandar
* Redireccionamiento
* Alias

Y en general ilustra con ejemplos.
```bash
touch ejemplo
cat ejemplo
ls -A | greap e?e?*plo | xargs rm
```

## Criptografía Simétrica y Asimétrica en la practica
__Taller__ que explica diferentes formas de _cifrar datos_ y algunas prácticas habituales en el mundo de la seguridad, nos explica como funcionan:
* [Criptografía simétrica](https://es.wikipedia.org/wiki/Criptograf%C3%ADa_sim%C3%A9trica)
* [Criptografía asimétrica](https://es.wikipedia.org/wiki/Criptograf%C3%ADa_asim%C3%A9trica)
* [Hash](https://es.wikipedia.org/wiki/Funci%C3%B3n_hash)
* [Firma digital](https://es.wikipedia.org/wiki/Firma_digital)

Además nos presenta con algunos ejemplos de casos de uso _reales y muy ilustrativos_ que completan la explicación y ayudan a comprender mejor los conceptos.

## Librerías para Web Scraping
__Taller__ que nos presenta los conceptos __básicos__ del web scraping usando [_`python`_](https://www.python.org/), en este __taller__ se nos presentarán las librerias:
* [Request II](https://docs.python-requests.org/en/master/)
* [Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/)

Junto a algunos ejemplos de extracción de datos que ayudarán a ilustrar la explicación.

## Curso de Web scraping: Extracción de datos en la Web
__Curso__ que nos explica conceptos de la utilización de [_python_](https://www.python.org/) para web scraping, explicando conceptos y presentando librerías.
Duranto el __curso__ se utilizarán:
* Sockets
* [Urllib.request](https://docs.python.org/3/library/urllib.request.html#module-urllib.request)
* [Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/)
* [Selenium web driver](https://www.selenium.dev/)

Y se explicarán conceptos como:
* JSON
* XML
* Web crawlers
* Protocolo HTTP
* Scraping
* Parsing
* API

La parte final del curso se centrará en ilustrar __no solo el web scraping con ejemplos si no también el web crawling y el uso de API's__.

```python
import urllib.request
from bs4 import BeautifulSoup

url = "https://www.example.com"

#Se realiza la petición a la página.
html = urllib.request.urlopen(url).read()

#Se convierte el html a un objeto BeautifulSoup.
soup = BeautifulSoup(html,"html.parser")

#Se muestra el documento conseguido.
print(html)

#Se muestra el documento conseguido y formateado por Beautiful soup.
print(soup)
```

## Curso de introducción al testing
__Curso enteramente teórico__ en el que se nos explican los conceptos teóricos del testing y la función de los _QA_.
Se nos presentan y explican:
* Niveles de testing
  * Test unitarios
  * Test de integración
  * Pruebas de sistema
  * Pruebas de aceptación
* Tipos de pruebas
  * Pruebas funcionales
  * Pruebas no funcionales
  * Pruebas de rendimiento
* Técnicas de testing
  * Pruebas de caja blanca
  * Pruebas de caja negra
  * Alfa/Beta testing y pruebas basadas en la experiencia
* Herramientas para la gestión de pruebas
  * Regresión
  * Herramientas de gestión de casos de prueba
  * Organización de los casos de prueba
  * Reportes
  * Automatización teórica
  * Automatización práctica

## Herramientas de testing para un desarrollador Java
__Taller__ en el que se nos presentan herramientas para el testing en _java_ y algunos ejemplos de uso real, algunas de estas herramientas son:
* [AssertJ](https://joel-costigliola.github.io/assertj/index.html)
* [Junit](https://junit.org/junit5/)
* [Awaitility](http://www.awaitility.org/)
* [Rest assured](https://github.com/rest-assured/rest-assured)
* [Hoverfly](https://hoverfly.io/)
* [Docker](https://www.docker.com/)

## Curso de TDD: Test-driven development
__Curso__ en el que se nos explica y expone la diferencía entre el flujo de trabajo _clásico_ en un proyecto y la filosifía TDD, en la cual se planifican y se codifican las pruebas antes de escribir el código siguiendo una planificación la cual garantiza que el software haga lo que el cliente necesita.
Se nos explican con ejemplos:
* Flujo de trabajo de un proyecto con TDD
* Algoritmo del TDD
* Frameworks de testeo
* Desarrollo dirigido por tests de aceptación (ATDD)
* Errores comunes y antipatrones
* Ventajas y desventajas del TDD
 
```java
 //Usando JUnit
 @Test
    public void comprobarEjemplo(){
        Ejemplo ej = new Ejemplo();
        ej.generarEjemplo();
        assertEquals("Buen ejemplo",ej.getEjemplo());
    }
```
## Experto en bases de datos
### Curso SQL desde cero
Curso que proporciona conocimiento de las consultas **básicas** del lenguaje sql además de explicar *conceptos y nociones **básicas*** como:
+ SGBD
+ DML
+ DDL
+ DCL

Algunas de las consultas **básicas** son:
+ Select: 
```sql
select * from usuarios;
```
+ Update:
```sql
update usuarios set practica = true;
```
+ Union
```sql
select nombre apellidos from usuario union select nombre apellidos from administradores;
```
+ Join
```sql
select jugador.nombre, ganador.puntuacion from jugador join ganador on jugador.id = ganador.id;
```

Aunque, por supuesto, se ilustra con ejemplos.
```sql
select * from ejemplo where creadorEjemplo = 'Kojima';
```

### Creación y administración de bases de datos
Curso en el que se proporciona un *repaso de las sentencias vistas en el anterior* además de presentar conceptos como **diagrama entidad relación**.
![Diagrama entidad relación](ExpertoEnBasesDeDatos/Bases de datos relacionales/diagramaDeCompras.png)
![Diagrama entidad relación](ExpertoEnBasesDeDatos/Bases de datos relacionales/Base de datos compra.png)

Las consultas en las que se profundizará en este curso son:
+ Create:
```sql
create database ejemplo;
create table if not exists tablaEjemplo(
    ID smallint unsigned not null auto_increment primary key,
    CampoDeEjemplo varchar(10)
)ENGINE INNODB;
``` 
+ Insert:
```sql
insert into tablaEjemplo (CampoDeEjemplo) values ('texto');
```
+ Update:
```sql
update tablaEjemplo set CampoDeEjemplo = 'texto2' where CampoDeEjemplo like 'texto';
```
+ Delete: 
```sql
delete from tablaEjemplo where CampoDeEjemplo like '?*?A'
```
+ Truncate:
```sql
truncate table tablaEjemplo;
```
+ Alter:
```sql
alter table tablaEjemplo add CampoDeEjemplo2 bigint not null;
```
+ Drop:
```sql
drop table tablaEjemplo;
drop database ejemplo;
```
## Curso de PostgreSQL: Instalación, configuración y optimización
__Curso__ que nos explica como funciona [postgreSQL](https://www.postgresql.org/), y sus características:
* Tipos de seguridad a nivel de conectividad de cluster
* Seguridad por filas
* Extensiones PostgreSQL
* Recuperación ante desastres
* Rendimiento

Además nos proporcionará conocimiento de como personalizar nuestra instalación para que se ajuste a nuestro caso de uso con algunos ejemplos de casos que hapreparado el profesor.

Siendo postgreSQL un sistema gestor que utiliza el lenguage SQL las consultas no varian en gran medida y se podría decir que son casi iguales a otros sistemas gestores SQL y por lo tanto este curso se centra en la teória particular de postgeSQL y en la práctica de como configurarlo.

### Curso de Cassandra
__Curso__ que nos introduce al mundo del _not only SQL o NOSQL_, nos explica conceptos de este mundo, el porque existen este tipo de bases de datos y cuales son sus diferencías con las bases de datos SQL.
Este curso se centra en [Apache Cassandra](https://cassandra.apache.org/) y nos explica:
* Arquitectura
* Modelado
* CQL (Cassandra Query Language)
```CQL
create keyspace espacioEjemplo
 with replication = {'class':'SimpleStrategy','replication_factor':2};
use espacioEjemplo;
```

### Curso de MongoDB: Creación y gestión de bases de datos NoSQL
__Curso__ que una vez más nos da una explicación básica del mundo de las bases de datos NOSQL, además de explicar el formato JSON debido a que [mongoDB](https://www.mongodb.com/es) es una base de datos NOSQL orientada a documentos y por los tanto utiliza el formato JSON para almecenar información y mostrarla, además de tener una shell de [_javascript_](https://www.javascript.com/) para escribir scripts que se le pueden pasar a la base de datos para que los ejecute.
En este __curso__ se nos presenta como trabaja mongoDB además de los comandos necesarios para gestionar nuestra base de datos y mantenerla, también se nos explica el concepto de [_schemaless_](https://www.mongodb.com/unstructured-data/schemaless) el cual es una de las grandes diferencias entre las bases de datos SQL y mongoDB, algunas equivalencias son:
| MariaDB | MongoDB |
|---------|---------|
| create database ejemplo; | use ejemplo|
|use ejemplo; | use ejemplo|
|select * from ejemplo;| db.ejemplo.find() o [db.ejemplo.find().pretty()](https://docs.mongodb.com/manual/reference/method/cursor.pretty/)|
| insert into ejemplo (nombre) values ("ejemplo"); | db.ejemplo.insertOne({"Nombre":"Ejemplo"})|
|drop database ejemplo;| db.dropDatabase() (mientras se está usando la base de datos ejemplo)|


