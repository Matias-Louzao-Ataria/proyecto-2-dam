import socket
from bs4 import BeautifulSoup
#Creación del mi socket de tipo stream y ipv4
customSocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
#Conexión del socket
customSocket.connect(('duckduckgo.com',80))
#Envio de la petición
customSocket.send("GET https://duckduckgo.com HTTP/1.0\n\n".encode())
html = ""
#Recepción de datos
while True:
    #Recepción de 1024 bytes
    data = customSocket.recv(1024)
    #Comprobación de que sigo reciviendo datos.
    if len(data) <= 0:
        break
    else:
        #Almacenamiento de los datos en una variable.
        html += data.decode()
#Mostrar la variable.
#print(html)

#Conversión del html a un objeto BeautifulSoup para poder extraer datos.
mysoup = BeautifulSoup(html,'html.parser')
#Obtención de las etiquetas 'a' del html
tags = mysoup('a')

#Mostrar diferentes características de las etiquetas de beautifulSoup
for tag in tags:
    print(tag.name)
    print(tag.text)
    print(tag.attrs)
    print(tag['href'])
    print(tag.attrs['class'])
    print("\n")

#Cierre del socket
customSocket.close()