import socket
#Creación del mi socket de tipo stream y ipv4
customSocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
#Conexión del socket
customSocket.connect(('www.w3.org',80))
#Envio de la petición
customSocket.send("GET https://www.w3.org/TR/PNG/iso_8859-1.txt HTTP/1.0\n\n".encode())
#Recepción de datos
while True:
    #Recepción de 1024 bytes
    data = customSocket.recv(1024)
    #Comprobación de que sigo reciviendo datos.
    if len(data) <= 0:
        break
    else:
        #Mostrar datos recividos
        print(data.decode())
#Cierre del socket
customSocket.close()