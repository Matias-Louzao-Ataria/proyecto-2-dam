import urllib.request
import json

#Se pide al usuario la ciudad de la que desea consultar el tiempo.
ciudad = input("Cuidad de la que se desea consultar el tiempo:")
#Se prepara la url de la petición.
url = "https://api.openweathermap.org/data/2.5/weather?q="+ciudad+"&appid=55d48b4fcda9c6890854dcb86d033c7c"
#Se realiza la petición.
datosRecividos = urllib.request.urlopen(url).read().decode()
#Se convierten los datos recividos a un objeto JSON.
jsonRecivido = json.loads(datosRecividos)
#Se recorre el objeto JSON en busca de la información deseada.
print("Nombre de la ciudad:")
print(jsonRecivido["name"])
print("País en el que se encuentra la cuidad:")
print(jsonRecivido["sys"]["country"])
print("coordenadas de la ciudad:")
print("latitud:")
print(jsonRecivido["coord"]["lat"])
print("longitud:")
print(jsonRecivido["coord"]["lon"])
print("descripción del tiempo:")
print(jsonRecivido["weather"][0]["description"])


