import urllib.request
from bs4 import BeautifulSoup

#Se prepara la petición a la página de la noticia .
url = "https://www.elmundo.es/ciencia-y-salud/medio-ambiente/2021/05/11/60994e1efdddffea178b4598.html"
#Se realiza la petición a la página.
html = urllib.request.urlopen(url).read()
#Se convierte el html a un objeto BeautifulSoup.
soup = BeautifulSoup(html,"html.parser")
#Se buscan los tags <p> que son los que contienen el texto de la noticia.
tags = soup("p")
#Se muestra el texto de la noticia contenido en los tags <p>.
for tag in tags:
    print(tag.text)