package com.junit.matias;

import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class AppTest {

    @Test
    public void ingresar100AUnaCuentaCon0Saldo(){
        Cliente c = new Cliente();
        c.ingresar(100d);
        assertEquals(100,c.getSaldo());
    }

    @Test
    public void ingresarSaldoNegativoAUnaCuentaCon0Saldo(){
        //Arrange(Set Up)
        Cliente c = new Cliente();

        //Act
        c.ingresar(-1500d);

        //Assert
        assertEquals(0,c.getSaldo());
    }


    @Test
    public void ingresarSaldoConDosDecimalesACuentaCon100DeSaldo(){
        //Arrange (Set up)
        Cliente c = new Cliente();
        c.ingresar(100d);

        //Act
        c.ingresar(90.50);

        //Assert
        assertEquals(190.50,c.getSaldo());
    }

    @Test
    public void ingresarSaldoConTresDecimalesACuentaCon100DeSaldo(){
        //Arrange (Set up)
        Cliente c = new Cliente();
        c.ingresar(100d);

        //Act
        c.ingresar(90.501);

        //Assert
        assertEquals(100,c.getSaldo());
    }

    @Test
    public void retirarMasSaldoDelDisponible(){
        //Arrange (Set up)
        Cliente c = new Cliente();
        c.ingresar(10d);

        //Act
        c.retirar(100d);

        //Assert
        assertEquals(10,c.getSaldo());
    }

    @Test
    public void retirarSaldoNegativo(){
        //Arrange (Set up)
        Cliente c = new Cliente();
        c.ingresar(10d);

        //Act
        c.retirar(-100d);

        //Assert
        assertEquals(10,c.getSaldo());
    }

    @Test
    public void retirarSaldoConDosDecimales(){
        //Arrange (Set up)
        Cliente c = new Cliente();
        c.ingresar(1000d);

        //Act
        c.retirar(100.05d);

        //Assert
        assertEquals(899.95,c.getSaldo());
    }

    @Test
    public void retirarSaldoConTresDecimales(){
        //Arrange (Set up)
        Cliente c = new Cliente();
        c.ingresar(1000d);

        //Act
        c.retirar(100.015d);

        //Assert
        assertEquals(1000,c.getSaldo());
    }

    @Test
    public void transferirMasSaldoDelDisponible(){
        //Arrange (Set up)
        Cliente c = new Cliente();
        Cliente destino = new Cliente();
        c.ingresar(10d);

        //Act
        c.transferir(100d,destino);

        //Assert
        assertEquals(10,c.getSaldo());
        assertEquals(0,destino.getSaldo());
    }

    @Test
    public void transferirSaldoNegativo(){
        //Arrange (Set up)
        Cliente c = new Cliente();
        Cliente destino = new Cliente();
        c.ingresar(10d);

        //Act
        c.transferir(-100d,destino);

        //Assert
        assertEquals(10,c.getSaldo());
        assertEquals(0,destino.getSaldo());
    }

    @Test
    public void transferirSaldoConDosDecimales(){
        //Arrange (Set up)
        Cliente c = new Cliente();
        Cliente destino = new Cliente();
        c.ingresar(1000d);

        //Act
        c.transferir(100.05d,destino);

        //Assert
        assertEquals(899.95,c.getSaldo());
        assertEquals(100.05,destino.getSaldo());
    }

    @Test
    public void transferirSaldoConTresDecimales(){
        //Arrange (Set up)
        Cliente c = new Cliente();
        Cliente destino = new Cliente();
        c.ingresar(1000d);

        //Act
        c.transferir(100.015d,destino);

        //Assert
        assertEquals(1000,c.getSaldo());
        assertEquals(0,destino.getSaldo());
    }
}
