package com.junit.matias;

public class Cliente {
    private double saldo = 0;

    public Cliente() {
        
    }
    
    public void ingresar(Double cantidad){
        if(cantidad > 0){
            String[] n = cantidad.toString().split("\\."); 
            if(n[1].length() <= 2){
                this.saldo += cantidad;
            }
        }
    }

    public void transferir(Double cantidad,Cliente destinatario){
        if(cantidad > 0){
            if(cantidad <= this.saldo){
                this.retirar(cantidad);
                destinatario.ingresar(cantidad);
            }
        }
    }

    public void retirar(Double cantidad){
        if(cantidad > 0){
            if(cantidad <= this.saldo){
                String[] n = cantidad.toString().split("\\."); 
                if(n[1].length() <= 2){
                    this.saldo -= cantidad;
                }
            }
         }
    }

    public double getSaldo(){
        return this.saldo;
    }
}
