create role usuarioTest with login createrole password 'passwd';

create role grupoTest with createdb login password null admin usuarioTest;

create database base1;

grant all on database base1 to usuariotest;

grant grupoTest to postgres with admin option; 

revoke postgres from usuarioTest;

drop database base1;