create tablespace miEspacioDeTabla location '/var/lib/postgres/miEspacioDeTabla';

create database base1 with template = 'template0' LC_COLLATE = 'es_ES.UTF-8' LC_CTYPE = 'es_ES.UTF-8' tablespace = 'miespaciodetabla';

create database base2;

drop database base1;

drop database base2;