create database db1;

create schema esquema1;

create table if not exists tabla1 (
	id1 integer primary key
);

create role db1_admin with login password null;

create role db1_ro with login password null;

create role db1_rw with login password null;

revoke all on database db1 from public;

REVOKE creation ON SCHEMA esquema1 FROM public;

GRANT CONNECT CREATE ON database db1 TO db1_admin;

GRANT CONNECT ON database db1 TO db1_ro;

GRANT CONNECT ON database db1 TO db1_rw;

GRANT ALL ON SCHEMA esquema1 TO db1_admin;

GRANT SELECT ON ALL tables IN SCHEMA esquema1 TO db1_admin;

GRANT SELECT ON ALL tables IN SCHEMA esquema1 TO db1_ro;

GRANT SELECT,INSERT,UPDATE,DELETE ON ALL tables IN SCHEMA esquema1 TO db1_rw;

ALTER default PRIVILEGES IN SCHEMA esquema1 GRANT ALL ON TABLES TO db1_admin;

ALTER default PRIVILEGES IN SCHEMA esquema1 GRANT Select ON TABLES TO db1_ro;

ALTER default PRIVILEGES IN SCHEMA esquema1 GRANT SELECT,UPDATE,INSERT,DELETE ON TABLES TO db1_rw;

CREATE TABLE IF NOT EXISTS tabla2(
 	id1 integer PRIMARY KEY
);