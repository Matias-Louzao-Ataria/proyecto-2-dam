use baseEjemplo

db.ejemplo.insertMany([{"Nombre":"A"},{"Nombre":"B","DNI":"12345678A"},{"Nombre":"C"}])

db.ejemplo.find().pretty()

db.ejemplo.deleteOne({"Nombre":"B"})

db.ejemplo.find().pretty()

db.ejemplo.deleteMany({})//Elimina todo lo que esté en la base de datos.

db.ejemplo.find().pretty()

db.ejemplo.insertOne({"Nombre":"A"})

db.ejemplo.find().pretty()

db.ejemplo.updateOne({"Nombre":"A"},{$set:{"Nombre":"B"}})

db.ejemplo.find().pretty()

db.ejemplo.deleteMany({})

db.ejemplo.insertMany([{"Nombre":"A"},{"Nombre":"A","DNI":"12345678A"},{"Nombre":"A"}])

db.ejemplo.find().pretty()

db.ejemplo.updateMany({"Nombre":"A"},{$set:{"Nombre":"B"}})

db.ejemplo.find().pretty()

db.ejemplo.replaceOne({"Nombre":"B"},{"Nombre":"C"})

db.ejemplo.find().pretty()

db.dropDatabase()

show dbs
