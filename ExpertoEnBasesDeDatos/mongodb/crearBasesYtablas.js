//Para crear una base de datos basta con querer usarla.
use baseDeEjemplo

//Para mostrar mis bases de datos.
show dbs

//Si no se inserta un dato en la base de datos, esta no aparece y no está disponible.

//Se crea una tabla al querer usuarla.
db.ejemplo.insertOne({nombre:"nombreDeEjemplo"})

show dbs

//Elimino la base de datos de ejemplo.
db.dropDatabase()

show dbs
