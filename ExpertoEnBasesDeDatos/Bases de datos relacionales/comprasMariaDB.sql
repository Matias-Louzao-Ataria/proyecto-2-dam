CREATE database IF NOT EXISTS compra;

use compra;

CREATE table if not exists compra(
	IDCompra smallint unsigned not null auto_increment primary key,
	TotalEstimado smallint unsigned,
	DiaCompra timestamp not null default current_timestamp
)ENGINE INNODB;

create table if not exists producto(
	IDProducto smallint unsigned not null auto_increment primary key,
	Nombre varchar(20) not null,
	Marca varchar(20),
	Precio smallint not null,
	FrecuenciaCompra smallint
)ENGINE INNODB;

create table if not exists cierre(
	IDHoraCierre smallint unsigned not null auto_increment primary key,
	Hora time not null
)ENGINE INNODB;

create table if not exists tienda(
	IDTienda smallint unsigned not null auto_increment primary key,
	IDHoraCierre SMALLINT UNSIGNED NOT NULL,
	CONSTRAINT HoraCierre_FK FOREIGN KEY (IDHoraCierre) REFERENCES compra.cierre(IDHoraCierre) ON DELETE CASCADE
)ENGINE INNODB;

create table if not exists compra_producto(
	IDCompra smallint unsigned not null,
	IDProducto smallint unsigned  not null,
	constraint Compra_FK foreign key (IDCompra) references compra(IDCompra),
	constraint Producto_FK foreign key (IDProducto) references producto(IDProducto)
)ENGINE INNODB;

create table if not exists tienda_producto(
	IDTienda smallint unsigned not null,
	IDProducto smallint unsigned not null,
	constraint Tienda_FK foreign key (IDTienda) references tienda(IDTienda),
	constraint Tienda_Producto_FK foreign key (IDProducto) references producto(IDProducto)
)ENGINE INNODB;